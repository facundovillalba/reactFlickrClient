import React from 'react';
import { StyleSheet, Image, Text, View, Linking, FlatList } from 'react-native';
import requestURL from './constants/FlickrApiConstants'
import * as stringConstants from './constants/StringResources';

export default class PhotoDetail extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('photo', 'undefiedPhoto').title
    };
  };

  constructor(props) {
    super(props)
    this.state = {
      photo: this.props.navigation.getParam('photo', 'undefinedPhoto'),
      comments: [],
      loading: true,
    };
    this.loadComments();
  }
  loadComments() {
    let method = 'flickr.photos.comments.getList';
    let url = `${requestURL}&method=${method}&photo_id=${this.state.photo.id}`;
    return fetch(url).then((res) => res.json()).then((jsonRes) => this.handleloadCommentsResponse(jsonRes));
  };

  handleloadCommentsResponse(res) {
    //TODO error handling  
    this.setState({ comments: res.comments.comment })
  }

  renderItem = ({ item }) => {
    return (
      <View style={[styles.container, { marginLeft: '5%' }]} >
        <Text style={styles.authorname}>
          {item.authorname}
        </Text>
        <Text style={styles.descriptionText}>
          {item._content}
        </Text>
      </View>
    );
  }

  renderEmptyList = () => {
    return (
      <View>
        <ImageBackground
          source={require("../assets/emptyList.png")}
          resizeMode='cover'
          style={{ flex: 1 }}>
        </ImageBackground>
      </View>
    );
  }

  renderSeparator = () => {
    return (
      <ListSpearator />
    );
  };

  render() {
    let openInBrowser = stringConstants.OPEN_IN_BROWSER;
    let noComments = stringConstants.NO_COMMENTS;

    return (
      <View style={styles.container}>
        <View style={{ backgroundColor: 'black', flex: 1 }}>
          <Image source={{ uri: this.state.photo.url_m }} style={{ flex: 1, }} />
        </View>
        <View style={{ flexDirection: 'row', borderBottomColor: '#CED0CE', borderBottomWidth: StyleSheet.hairlineWidth, }} >
          <Image source={require("../assets/openInBrowser.png")} style={{
            resizeMode: 'contain',
            width: 30,
            height: 30,
            marginTop: 5
          }} />
          <Text style={{ fontSize: 22, letterSpacing: 1, marginLeft: 15 }}
            onPress={() => { Linking.openURL(this.state.photo.url_m) }}>
            {openInBrowser}
          </Text>
        </View >
        <View style={styles.container}>
          {
            this.state.comments ?

              (<View>
                <FlatList
                  ItemSeparatorComponent={this.renderSeparator}
                  keyExtractor={item => item.id}
                  data={this.state.comments}
                  renderItem={this.renderItem}
                />
              </View >)
              :
              (
                <View style={styles.emptyListContainer}>
                  <Image
                    style={{ flex: 0.5, resizeMode: 'contain' }}
                    source={require("../assets/emptyList.png")} />
                  <Text style={{ flex: 0.5, textAlign: 'center' }}> {noComments} </Text>
                </View>
              )
          }
        </View>
      </View>
    );
  };
}

function ListSpearator() {
  return (
    <View style={{ height: 1, width: "98%", alignSelf: "center", backgroundColor: "#CED0CE" }} />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  authorname: {
    fontSize: 20,
    flex: 0.8,
    flexWrap: 'wrap',
    fontWeight: 'bold',
  },
  descriptionText: {
    fontSize: 18,
    flex: 0.8,
    flexWrap: 'wrap'
  },
  emptyListContainer: {
    marginTop: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    height: '80%'
  },
});